/*
     This file is part of anastasis-gtk.
     Copyright (C) 2021 Anastasis SARL

     Anastasis is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Anastasis is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Anastasis; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/anastasis/print.c
 * @brief Printing helper.
 * @author Christian Grothoff
 */

#include <setjmp.h>
#include <hpdf.h>
#include <gnunet/gnunet_util_lib.h>
#include "print.h"


static void
error_handler  (HPDF_STATUS error_no,
                HPDF_STATUS detail_no,
                void         *user_data)
{
  jmp_buf *env = user_data;

  GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
              "error_no=%04X, detail_no=%u\n",
              (HPDF_UINT) error_no,
              (HPDF_UINT) detail_no);
  longjmp (*env,
           1);
}


int
AG_print (json_t *user,
          const char *fname)
{
  const char *page_title = "GNU Anastasis Identity Sheet";
  const char *type;
  json_t *jvalue;
  HPDF_Doc pdf;
  HPDF_Font font;
  HPDF_Page page;
  float tw;
  float fsize;
  jmp_buf env;


  pdf = HPDF_New (&error_handler,
                  &env);
  if (! pdf)
  {
    printf ("error: cannot create PdfDoc object\n");
    return 1;
  }

  if (setjmp (env))
  {
    HPDF_Free (pdf);
    return 1;
  }

  /* set compression mode */
  HPDF_SetCompressionMode (pdf,
                           HPDF_COMP_ALL);
  /* create default-font */
  font = HPDF_GetFont (pdf,
                       "Helvetica",
                       NULL);
  /* add a new page object. */
  page = HPDF_AddPage (pdf);

  /* print the lines of the page. */
  HPDF_Page_SetLineWidth (page, 1);
  HPDF_Page_Rectangle (page,
                       50, 50,
                       HPDF_Page_GetWidth (page) - 100,
                       HPDF_Page_GetHeight (page) - 110);
  HPDF_Page_Stroke (page);

  /* print the title of the page (with positioning center). */
  HPDF_Page_SetFontAndSize (page,
                            font,
                            24);
  tw = HPDF_Page_TextWidth (page,
                            page_title);
  HPDF_Page_BeginText (page);
  HPDF_Page_TextOut (page,
                     (HPDF_Page_GetWidth (page) - tw) / 2,
                     HPDF_Page_GetHeight (page) - 50,
                     page_title);
  HPDF_Page_EndText (page);

  HPDF_Page_BeginText (page);
  HPDF_Page_MoveTextPos (page, 60, HPDF_Page_GetHeight (page) - 60);

  /*
   * font size
   */
  fsize = 18;
  json_object_foreach (user, type, jvalue)
  {
    char *val;
    int len;

    /* print the description. */
    HPDF_Page_MoveTextPos (page, 0, -12);
    HPDF_Page_SetFontAndSize (page, font, 10);
    HPDF_Page_ShowText (page, type);

    /* set the position of the text. */
    HPDF_Page_MoveTextPos (page, 10, -5 - fsize);
    val = GNUNET_strdup (json_string_value (jvalue));
    do {
      /* set style and size of font. */
      HPDF_Page_SetFontAndSize (page, font, fsize);
      /* measure the number of characters which included in the page. */
      len = HPDF_Page_MeasureText (page,
                                   val,
                                   HPDF_Page_GetWidth (page) - 130,
                                   HPDF_FALSE,
                                   NULL);
      if ( (len < (ssize_t) strlen (val)) &&
           (fsize > 10) )
        fsize--;
      else
        break;
    } while (1);
    /* truncate the text. */
    val[len] = 0x00;
    HPDF_Page_ShowText (page, val);
    GNUNET_free (val);
    HPDF_Page_MoveTextPos (page, -10, -25 - fsize);
  }

  HPDF_Page_EndText (page);

  /* save the document to a file */
  HPDF_SaveToFile (pdf, fname);

  /* clean up */
  HPDF_Free (pdf);

  return 0;
}
