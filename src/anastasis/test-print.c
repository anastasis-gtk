/*
     This file is part of anastasis-gtk.
     Copyright (C) 2021 Anastasis SARL

     Anastasis is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Anastasis is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Anastasis; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/anastasis/test-print.c
 * @brief Program to test printing helper.
 * @author Christian Grothoff
 */
#include "print.h"


int
main (int argc,
      char **argv)
{
  json_t *user;
  int ret;

  user = json_pack ("{s:s,s:s,s:s}",
                    "key1", "value1",
                    "key2",
                    "superloooooooooooooooooooooooooooooooooooooooooooooooooongvaaaaluevalue2",
                    "key3", "value3");
  ret = AG_print (user,
                  "test-print.pdf");
  json_decref (user);
  return ret;
}
