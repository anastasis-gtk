/*
     This file is part of anastasis-gtk.
     Copyright (C) 2020-2022 Anastasis SARL

     Anastasis is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Anastasis is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Anastasis; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/anastasis/anastasis-gtk.h
 * @brief Definitions of the main Gtk GUI.
 * @author Christian Grothoff
 * @author Dennis Neufeld
 */
#ifndef ANASTASIS_GTK_H
#define ANASTASIS_GTK_H
#include <gnunet-gtk/gnunet_gtk.h>
#include <gtk/gtk.h>
#include <anastasis/anastasis_service.h>
#include <anastasis/anastasis_redux.h>

/**
 * Handle to our main loop.
 */
extern struct GNUNET_GTK_MainLoop *AG_ml;

/**
 * Active policy discovery job, or NULL.
 */
extern struct ANASTASIS_PolicyDiscovery *AG_pd;

/**
 * Application ID to include in the user attributes.
 */
extern char *AG_application_id;

/**
 * Our configuration.
 */
extern const struct GNUNET_CONFIGURATION_Handle *AG_cfg;

/**
 * Hash map from UUID hashes to GtkWidgets.
 */
extern struct GNUNET_CONTAINER_MultiHashMap *AG_entry_attributes;

/**
 * Actual state.
 */
extern json_t *AG_redux_state;

/**
 * Handle to an ongoing action.
 */
extern struct ANASTASIS_ReduxAction *AG_ra;

/**
 * Curl context for communication with taler backend
 */
extern struct GNUNET_CURL_Context *AG_ctx;

/**
 * State associated with a background long action.
 */
struct ANASTASIS_LongAction
{
  /**
   * Action handle of the background action.
   */
  struct ANASTASIS_ReduxAction *ra;

  /**
   * Handle to task to reschedule the action.
   */
  struct GNUNET_SCHEDULER_Task *task;

  /**
   * Next time we schedule the task.
   */
  struct GNUNET_TIME_Absolute next_time;

};


/**
 * Enumeration for long poll actions we have.
 */
enum ANASTASIS_LongActionKind
{
  /**
   * Poll on async challenge completion.
   */
  ANASTASIS_LP_POLL_CHALLENGES,

  /**
   * Poll for /config on all known providers.
   */
  ANASTASIS_LP_SYNC_PROVIDERS,

  /**
   * Wait for challenge-specific providers being ready.
   */
  ANASTASIS_LP_POLL_PROVIDERS,

  /**
   * Number of pollers we have.
   */
  ANASTASIS_LP_CNT /* must be last! */
};

/**
 * Handle to an ongoing background action.
 */
extern struct ANASTASIS_LongAction AG_lacs[ANASTASIS_LP_CNT];


/**
 * Stop long polling action in the background.
 * Should be called whenever we leave the
 * challenge-selecting state.
 */
void
AG_stop_long_action (void);


/**
 * Load #AG_redux_state from @a filename.
 *
 * @param filename to load
 */
void
AG_load (const char *filename);

#endif
