/*
     This file is part of anastasis-gtk.
     Copyright (C) 2021 Anastasis SARL

     Anastasis is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Anastasis is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Anastasis; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/anastasis/print.h
 * @brief Definition of printing helper.
 * @author Christian Grothoff
 */

#ifndef PRINT_H
#define PRINT_H

#include <jansson.h>


/**
 * Generate a PDF file with the key-value strings
 * from @a user and write it to @a fname.
 *
 * @param user user attribute data to output
 * @param fname filename to write the PDF to (will overwrite)
 * @return 0 on success
 */
int
AG_print (json_t *user,
          const char *fname);

#endif
