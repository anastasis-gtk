/*
     This file is part of anastasis-gtk.
     Copyright (C) 2020 Anastasis SARL

     Anastasis is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Anastasis is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Anastasis; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/anastasis/anastasis-gtk_noprint.c
 * @brief Implementation of the "print" button, if we cannot use it
 * @author Christian Grothoff
 */
#include <gnunet/gnunet_util_lib.h>
#include "anastasis-gtk_helper.h"
#include <jansson.h>
#include <gdk-pixbuf/gdk-pixbuf.h>


/**
 * The user clicked the 'print' button to print their personal
 * details. Try to print them.
 *
 * @param button the print button
 * @param user_data the builder for the dialog
 */
void
anastasis_gtk_print_details_button_clicked_cb (GtkButton *button,
                                               gpointer user_data)
{
  (void) button;
  (void) user_data;
  GNUNET_break (0); /* button should have been disabled */
}
