/*
     This file is part of Anastasis-Gtk.
     Copyright (C) 2023 Taler Systems SA

     Anastasis-Gtk is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Anastasis-Gtk is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Anastasis-Gtk; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file os_installation.c
 * @brief initialize libgnunet OS subsystem for Anastasis-Gtk.
 * @author Christian Grothoff
 */
#include "anastasis_gtk_config.h"
#include <gnunet/gnunet_util_lib.h>
#include "anastasis_gtk_util.h"

/**
 * Default project data used for installation path detection
 * for GNU Anastasis-Gtk.
 */
static const struct GNUNET_OS_ProjectData anastasis_gtk_pd = {
  .libname = "libanastasisgtkutil",
  .project_dirname = "anastasis-gtk",
  .binary_name = "anastasis-gtk",
  .env_varname = "ANASTASIS_GTK_PREFIX",
  .base_config_varname = "ANASTASIS_GTK_BASE_CONFIG",
  .bug_email = "anastasis@lists.gnu.org",
  .homepage = "https://anastasis.lu/",
  .config_file = "anastasis-gtk.conf",
  .user_config_file = "~/.config/anastasis-gtk.conf",
  .version = PACKAGE_VERSION,
  .is_gnu = 1,
  .gettext_domain = "anastasis-gtk",
  .gettext_path = NULL,
};


const struct GNUNET_OS_ProjectData *
ANASTASIS_GTK_project_data (void)
{
  return &anastasis_gtk_pd;
}


/* end of os_installation.c */
