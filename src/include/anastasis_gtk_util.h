/*
  This file is part of ANASTASIS-GTK
  Copyright (C) 2024 Taler Systems SA

  ANASTASIS-GTK is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  ANASTASIS-GTK is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  ANASTASIS-GTK; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/anastasis_gtk_util.h
 * @brief Interface for common utility functions
 * @author Christian Grothoff
 */
#ifndef ANASTASIS_GTK_UTIL_H
#define ANASTASIS_GTK_UTIL_H

#include <gnunet/gnunet_util_lib.h>

/**
 * Return project data used by Anastasis-Gtk.
 *
 * @return project data for anastasis-gtk
 */
const struct GNUNET_OS_ProjectData *
ANASTASIS_GTK_project_data (void);


#endif
